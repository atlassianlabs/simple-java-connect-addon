package com.atlassian.connect.starterapp.web.comments;

import java.util.List;

import com.atlassian.connect.starterapp.domain.Comment;
import com.atlassian.connect.starterapp.service.DefaultCommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/comments")
public class CommentsRestController {

    private DefaultCommentService service;

    @Autowired
    public CommentsRestController(DefaultCommentService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Comment> getComments() {
        return service.getComments();
    }

    @RequestMapping(method = RequestMethod.POST)
    public List<Comment> addComment(Comment comment) {
        return service.addComment(comment);
    }
}

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.4.0.RELEASE</version>
    </parent>

    <groupId>com.atlassian.connect</groupId>
    <artifactId>atlassian-connect-addon-starter-app</artifactId>
    <version>0.0.1</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>

        <maven.clean.plugin.version>3.0.0</maven.clean.plugin.version>
        <jsoup.version>1.7.3</jsoup.version>
        <lesscss.maven.plugin.version>1.7.0.1.1</lesscss.maven.plugin.version>
        <frontend.maven.plugin.version>1.2</frontend.maven.plugin.version>
        <node.version>v4.6.0</node.version>
        <atlassian-connect-spring-boot.version>1.3.0</atlassian-connect-spring-boot.version>
    </properties>

    <dependencies>
        <!-- Spring Boot -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional>
        </dependency>

        <!-- UI -->
        <dependency>
            <groupId>org.apache.tomcat.embed</groupId>
            <artifactId>tomcat-embed-jasper</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
        </dependency>

        <!-- Database -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- Atlassian Connect -->
        <dependency>
            <groupId>com.atlassian.connect</groupId>
            <artifactId>atlassian-connect-spring-boot-starter</artifactId>
            <version>${atlassian-connect-spring-boot.version}</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.connect</groupId>
            <artifactId>atlassian-connect-spring-boot-jpa-starter</artifactId>
            <version>${atlassian-connect-spring-boot.version}</version>
        </dependency>

        <!-- Testing -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>${jsoup.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <defaultGoal>spring-boot:run</defaultGoal>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <fork>true</fork>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-clean-plugin</artifactId>
                <version>${maven.clean.plugin.version}</version>
                <configuration>
                    <filesets>
                        <fileset>
                            <directory>src/main/resources/static/js/bower_components</directory>
                        </fileset>
                    </filesets>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.lesscss</groupId>
                <artifactId>lesscss-maven-plugin</artifactId>
                <version>${lesscss.maven.plugin.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <sourceDirectory>${project.basedir}/src/main/resources/static/less</sourceDirectory>
                    <outputDirectory>${project.build.directory}/classes/static/css</outputDirectory>
                    <compress>true</compress>
                    <includes>
                        <include>*.less</include>
                    </includes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.github.eirslett</groupId>
                <artifactId>frontend-maven-plugin</artifactId>
                <version>${frontend.maven.plugin.version}</version>
                <executions>
                    <execution>
                        <id>install node and npm</id>
                        <goals>
                            <goal>install-node-and-npm</goal>
                        </goals>
                        <phase>generate-resources</phase>
                    </execution>
                    <execution>
                        <id>npm install</id>
                        <goals>
                            <goal>npm</goal>
                        </goals>
                        <phase>generate-resources</phase>

                        <configuration>
                            <arguments>install</arguments>
                        </configuration>
                    </execution>
                    <execution>
                        <id>bower install</id>
                        <goals>
                            <goal>bower</goal>
                        </goals>
                        <phase>generate-resources</phase>

                        <configuration>
                            <arguments>--allow-root install</arguments>
                        </configuration>
                    </execution>
                    <execution>
                        <id>grunt build</id>
                        <goals>
                            <goal>grunt</goal>
                        </goals>
                        <phase>generate-resources</phase>
                    </execution>
                </executions>
                <configuration>
                    <nodeVersion>${node.version}</nodeVersion>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
